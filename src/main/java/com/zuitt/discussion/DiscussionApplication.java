package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
// Will require all routes within the "Discussion Application" to use "/greeting" as part of its routes
@RequestMapping("/greeting")
//The "@RestController annotation" tells Spring Boot that this application will function as an endpoint that will be used in handling web requests
public class DiscussionApplication {
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hello() {
		return "Hello World!";
	}

	// Routes with a string query
	// Dynamic data is obtained from the URL's string query
	// "%s" specifies that the value to be included in the format is of any data type
	// http://localhost:8080/hi?name=Joe

	@GetMapping("/hi")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	// Multiple Parameters
	// http://localhost:8080/friend?name=Ash&friend=Pikachu
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	// Routes with path variables
	@GetMapping("/hello/{name}")
	// http://localhost:8080/hello/joe
	// @PathVariable annotation allows us to extract data directly from the URL
	public String courses (@PathVariable ("name") String name){
		return String.format("Nice to meet you %s!", name);
	}


	// -------------------ACTIVITY s09-----------------------


	ArrayList<String> enrollees = new ArrayList<String>();

	// http://localhost:8080/greeting/enroll?user=Jerome
	@GetMapping("/enroll")
	public String addEnrollees(@RequestParam(value = "user") String user){
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}

	//http://localhost:8080/greeting/getEnrollees
	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	//  http://localhost:8080/greeting/nameage?name=Jill&age=31
	public String nameAge(@RequestParam(value = "name") String name, @RequestParam(value = "age") int age) {
		return String.format("Hello %s! My age is %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	// http://localhost:8080/greeting/courses/javaee101
	public String getCourses(@PathVariable("id") String id) {
		if(id.equals("java101")){
			return String.format("Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000");
		}
		else if (id.equals("sql101")){
			return String.format("Name: SQL 101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 4000");
		}
		else if(id.equals("javaee101")){
			return String.format("Name: JavaEE 101, Schedule: THF 1:30 PM - 4:00 PM, Price: PHP 5000");
		}
		else {
			return String.format("Course cannot be found");
		}
	}
}

